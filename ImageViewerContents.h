/*qAllInOne is a free open-source software built using Qt to provide users an All-In-One media player, so it can view images, play videos and audio.
qAllInOne Copyright (C) 2013  Mahmoud Jaoune

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef IMAGEVIEWERCONTENTS_H
#define IMAGEVIEWERCONTENTS_H

#include <QtWidgets>
#include <QPushButton>
#include <QMainWindow>
#include <QLabel>
#include <QSlider>
#include <QImage>

class ImageScreen : public QLabel
{
    Q_OBJECT

public:

    QPoint PointOffset;
    QWidget * Parent;

    explicit ImageScreen(QWidget * parent)
    {
        setParent(parent);
        Parent = parent;
    }

public slots:
    //Change cursor to Open Hand Cursor when zooming in is available and dragging image is possible
    void enterEvent(QEvent *event)
    {
        if(this->width() > Parent->width() && this->height() > Parent->height())
        {
            QApplication::setOverrideCursor(Qt::OpenHandCursor);
        }
    }

    //Change cursor to Default Pointer Cursor when mouse leaves the image area or dragging is not possible
    void leaveEvent(QEvent *event)
    {
        QApplication::setOverrideCursor(Qt::ArrowCursor);
    }

    //Change cursor to Closed Hand Cursor when Image is clicked to be dragged
    //Set the position of where the mouse clicked the image
    void mousePressEvent(QMouseEvent *event)
    {
        if(this->width() > Parent->width() && this->height() > Parent->height())
        {
            QApplication::setOverrideCursor(Qt::ClosedHandCursor);
            PointOffset = event->pos();
        }
    }

    //Change cursor to Open Hand Cursor when Image is released
    void mouseReleaseEvent(QMouseEvent *event)
    {
        if(this->width() > Parent->width() && this->height() > Parent->height())
        {
            QApplication::setOverrideCursor(Qt::OpenHandCursor);
        }
    }

    //Move image when mouse is moved (Drag)
    void mouseMoveEvent(QMouseEvent *event)
    {
        if(this->width() > Parent->width() && this->height() > Parent->height())
        {
            if(event->buttons() & Qt::LeftButton)
            {
                move(mapToParent(event->pos() - PointOffset));
            }
        }
    }

    //Limit image not to exceed bounds of MainWidget (Also MainWindow itself)
    void moveEvent(QMoveEvent *event)
    {
        if(this->width() > Parent->width())
        {
            if(x() >= 0)
            {
                setGeometry(0,y(),width(),height());
            }
            else if(x() + width() <= Parent->width())
            {
                setGeometry(Parent->width() - width(),y(),width(),height());
            }
        }

        if(this->height() > Parent->height())
        {
            if(y() >= 0)
            {
                setGeometry(x(),0,width(),height());
            }
            else if(y() + height() <= Parent->height())
            {
                setGeometry(x(),Parent->height() - height(),width(),height());
            }
        }

    }
    //Limit image not to exceed bounds of MainWidget (Also MainWindow itself) when resized (Zoomed in and out)
    void resizeEvent(QResizeEvent * event)
    {
        if(this->width() > Parent->width())
        {
            if(x() >= 0)
            {
                setGeometry(0,y(),width(),height());
            }
            else if(x() + width() <= Parent->width())
            {
                setGeometry(Parent->width() - width(),y(),width(),height());
            }
        }

        if(this->height() > Parent->height())
        {
            if(y() >= 0)
            {
                setGeometry(x(),0,width(),height());
            }
            else if(y() + height() <= Parent->height())
            {
                setGeometry(x(),Parent->height() - height(),width(),height());
            }
        }
    }
};

//Experimental code of the MiniImage and MiniFrame featured (Unimplemented)
//MiniImageFrame
class MiniImageFrame : public QWidget
{
    Q_OBJECT

public:
    QWidget * Background;
    QPoint PointOffset;
    QWidget * Parent;

    explicit MiniImageFrame(QWidget * parent)
    {
        setParent(parent);
        Parent = parent;
        Background = new QWidget(this);
        Background->setGeometry(0,0,this->width(),this->height());
        Background->setStyleSheet("* { background-color: rgb(255, 0, 0); }");
        setMouseTracking(true);
    }

signals:
    void FrameMoved(QPoint pos);

public slots:
    void mousePressEvent(QMouseEvent *event)
    {
        if(x() < 0)
        {
            PointOffset = QPoint(0,y());
        }
        if (y() < 0)
        {
            PointOffset = QPoint(x(),0);
        }
        else if((x() >= 0) && (y() >= 0)) PointOffset = event->pos();
    }

    void mouseMoveEvent(QMouseEvent *event)
    {
        if(event->buttons() & Qt::LeftButton)
        {
            if(x() < 0)
            {
                setGeometry(0,y(),width(),height());
            }
            if(y() < 0)
            {
                setGeometry(x(),0,width(),height());
            }

            else if((x() >= 0) && (y() >= 0))
            {
                move(mapToParent(event->pos() - PointOffset));
            }
        }
    }

    void moveEvent(QMoveEvent *event)
    {
        if(event->pos().x() <= 0)
        {
            setGeometry(0,y(),width(),height());
        }
        if(event->pos().y() <= 0)
        {
            setGeometry(x(),0,width(),height());
        }

        if(event->pos().x() >= (Parent->width() - width()))
        {
            setGeometry(Parent->width() - width(),y(),width(),height());
        }
        if(event->pos().y() >= (Parent->height() - height()))
        {
            setGeometry(x(),Parent->height() - height(),width(),height());
        }

        emit FrameMoved(QPoint(pos()));

    }


};

//MiniImage
class MiniImage : public QWidget
{
    Q_OBJECT

public:
    QLabel * Background;
    MiniImageFrame * MiniFrame;
    QImage ScaledImage;


    explicit MiniImage(QWidget * parent, QImage OriginalImage)
    {
        setParent(parent);

        Background = new QLabel(this);

        ScaledImage = OriginalImage.scaled(128,64,Qt::KeepAspectRatio,Qt::SmoothTransformation);

        setBaseSize(ScaledImage.width(),ScaledImage.height());

        Background->setPixmap(QPixmap::fromImage(ScaledImage,Qt::AutoColor));
        Background->setGeometry(0,0,ScaledImage.width(),ScaledImage.height());


        MiniFrame = new MiniImageFrame(Background);
        MiniFrame->setGeometry(0,0,ScaledImage.width(),ScaledImage.height());
    }


};

//End of unimplemented code

#endif // IMAGEVIEWERCONTENTS_H
