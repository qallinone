/*qAllInOne is a free open-source software built using Qt to provide users an All-In-One media player, so it can view images, play videos and audio.
qAllInOne Copyright (C) 2013  Mahmoud Jaoune

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtPlugin>
#include "ui_mainwindow.h"
#include <QMainWindow>
#include <QLabel>
#include <QString>
#include <QFile>
#include <QMovie>
#include <QImage>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QVideoWidget>
#include <QFileInfo>
#include <QGraphicsVideoItem>
#include <QGraphicsView>
#include <QMediaObject>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QMediaResource>
#include <QMediaMetaData>
#include <QResizeEvent>
#include <QAbstractVideoSurface>
#include "MediaToolbars.h"
#include <QDir>
#include <QMouseEvent>
#include <QScrollArea>
#include <QTabWidget>
#include "ImageViewerContents.h"
#include <cmath>
#include <QList>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    enum MMFiles
    {
        JPG,PNG,GIF,WMV,MP3,MP4,BMP,AVI
    };

    //Variables
public:
     ImageScreen * Screen;
     QGraphicsVideoItem * VideoInfo;
     QVideoWidget * VideoScreen;
     QMediaPlayer * VideoPlayer;
     QMediaPlayer * AudioPlayer;
     int ToolBarSpace;
     int ImageMargin;
     QString CurrentImage;
     QString CurrentAudio;
     QString CurrentVideo;
     ImageViewerToolBar *Toolb;
     VideoToolBar * VToolb;
     AudioToolBar * AToolb;

     MiniImage * ScaleImage;

     QString FileString;
     QStringList Dirlist;
     QString DirStack;
     QStringList ScanDirectory;
     QDir ScanDirs;

     int ImageShiftCounter;
     int ZoomCounter;

     QList<QImage> ScaledImageList;


     //Functions
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


    int AIO_ProcessMedia(QString Filename, MMFiles Filetype);

    QString IntToTime(int seconds);


public slots:
    //Audio Slots and settings

    void AMediaStatus(QMediaPlayer::MediaStatus status)
    {
        if(status == QMediaPlayer::LoadedMedia)
        {

        }

    }

    void AmetaDataAvailable(bool Available)
    {
        if(Available == true)
        {
            //Obtaining Video information
            QVariant CoverImage = (AudioPlayer->metaData(QMediaMetaData::CoverArtImage));
            QString Title = (AudioPlayer->metaData(QMediaMetaData::Title).toString());
            qint64 DurationMS = (AudioPlayer->metaData(QMediaMetaData::Duration).toInt());
            int Duration = DurationMS / 1000;

            if(!Title.isEmpty())
            {
                this->setWindowTitle(Title);
            }
            else this->setWindowTitle("qAllInOne - (" + CurrentAudio + ")");

            //Set Screen size and calibrate Window

            int OWidth = 471;
            int OHeight = 353;

            if(!QPixmap::fromImage(CoverImage.value<QImage>()).isNull())
            {
                OWidth = QPixmap::fromImage(CoverImage.value<QImage>()).width();
                OHeight = QPixmap::fromImage(CoverImage.value<QImage>()).height();
            }
            else
            {
                OWidth = 471;
                OHeight = 353;
            }

            //Calculate screen center and set MainWindow to center
            QDesktopWidget *desktop = QApplication::desktop();
            int CenterWidth = ((desktop->width() - OWidth) / 2);
            int CenterHeight = ((desktop->height() - OHeight) / 2);
            //Set MainWindow in Center
            this->setGeometry(CenterWidth,CenterHeight,OWidth,OHeight + ToolBarSpace);

            Screen->setPixmap(QPixmap::fromImage(CoverImage.value<QImage>()));

            Screen->setGeometry(Screen->x(),Screen->y(),OWidth,OHeight);

            Screen->show();

            AToolb->TimeSlider->setMaximum(Duration);

            AToolb->TotalTimeLabel->setText(IntToTime(Duration));

        }
        else
        {
            //In case the metadata was not found, use default properties
            QVariant CoverImage = (AudioPlayer->metaData(QMediaMetaData::CoverArtImage));
            QString Title = CurrentAudio;
            qint64 DurationMS = 0;
            int Duration = DurationMS / 1000;

            if(!Title.isEmpty())
            {
                this->setWindowTitle(Title);
            }
            else this->setWindowTitle("qAllInOne - (" + CurrentAudio + ")");

            //Set Screen size and calibrate Window

            int OWidth = 471;
            int OHeight = 353;

            //Calculate screen center and set MainWindow to center
            QDesktopWidget *desktop = QApplication::desktop();
            int CenterWidth = ((desktop->width() - OWidth) / 2);
            int CenterHeight = ((desktop->height() - OHeight) / 2);
            //Set MainWindow in Center
            this->setGeometry(CenterWidth,CenterHeight,OWidth,OHeight + ToolBarSpace);

            Screen->setPixmap(QPixmap::fromImage(CoverImage.value<QImage>()));

            Screen->setGeometry(Screen->x(),Screen->y(),OWidth,OHeight);


            Screen->show();

            AToolb->TimeSlider->setMaximum(Duration);

            AToolb->TotalTimeLabel->setText(IntToTime(Duration));
        }
    }

    void APlayVideo()
    {
        AudioPlayer->play();
    }
    void APauseVideo()
    {
        AudioPlayer->pause();
    }
    void AStopVideo()
    {
        AudioPlayer->stop();
        //AudioPlayer->pause();
        /*if(AudioPlayer->position() != AudioPlayer->duration())
        {
            AudioPlayer->pause();
            AudioPlayer->setPosition(0);
            AudioPlayer->pause();
        }*/
    }

    void AelapsedTimeChanged(qint64 mseconds)
    {
        int Seconds = mseconds / 1000;

        AToolb->ElapsedTimeLabel->setText(IntToTime(Seconds));
        AToolb->TimeSlider->setValue(Seconds);
    }

    void AchangePosition(int mseconds)
    {
        if(AudioPlayer->state() == QMediaPlayer::PausedState)
        {
            AudioPlayer->setPosition(mseconds * 1000);
        }
    }

    void AplayAfterChangePosition()
    {
        if(AudioPlayer->state() == QMediaPlayer::PausedState)
        {
            AudioPlayer->play();
        }
    }

    void ApauseBeforeChangePosition()
    {
        if(AudioPlayer->state() != QMediaPlayer::PausedState)
        {
            AudioPlayer->pause();
        }
    }

    void AchangeVolume(int vol)
    {
        AudioPlayer->setVolume(vol);
        AToolb->VolumeLabel->setText(QString::number(vol) + "%");
    }
    //End of Audio Slots and Settings

    //Video Slots and settings
    void VMediaStatus(QMediaPlayer::MediaStatus status)
    {
        if(status == QMediaPlayer::LoadedMedia)
        {

        }

    }

    void VmetaDataAvailable(bool Available)
    {
        if(Available == true)
        {
            //Obtaining Video information
            int OWidth = (VideoPlayer->metaData(QMediaMetaData::Resolution).toSize()).width();
            int OHeight = (VideoPlayer->metaData(QMediaMetaData::Resolution).toSize()).height();
            QString Title = (VideoPlayer->metaData(QMediaMetaData::Title).toString());
            qint64 DurationMS = (VideoPlayer->metaData(QMediaMetaData::Duration).toInt());
            int Duration = DurationMS / 1000;

            if(!Title.isEmpty())
            {
                this->setWindowTitle(Title);
            }
            else this->setWindowTitle("qAllInOne - (" + CurrentVideo + ")");

            //Calculate screen center and set MainWindow to center
            QDesktopWidget *desktop = QApplication::desktop();
            int CenterWidth = ((desktop->width() - OWidth) / 2);
            int CenterHeight = ((desktop->height() - OHeight) / 2);
            //Set MainWindow in Center
            this->setGeometry(CenterWidth,CenterHeight,OWidth,OHeight + ToolBarSpace);

            //Set VideoScreen size
            VideoScreen->setGeometry(VideoScreen->x(),VideoScreen->y(),OWidth,OHeight);

            VToolb->TimeSlider->setMaximum(Duration);

            VToolb->TotalTimeLabel->setText(IntToTime(Duration));

        }
        else
        {
            //In case the metadata was not found, use default properties
            int OWidth = 640;
            int OHeight = 480;
            QString Title = CurrentVideo;
            qint64 DurationMS = 0;
            int Duration = DurationMS / 1000;

            if(!Title.isEmpty())
            {
                this->setWindowTitle(Title);
            }
            else this->setWindowTitle("qAllInOne - (" + CurrentVideo + ")");

            //Calculate screen center and set MainWindow to center
            QDesktopWidget *desktop = QApplication::desktop();
            int CenterWidth = ((desktop->width() - OWidth) / 2);
            int CenterHeight = ((desktop->height() - OHeight) / 2);
            //Set MainWindow in Center
            this->setGeometry(CenterWidth,CenterHeight,OWidth,OHeight + ToolBarSpace);

            //Set VideoScreen size
            VideoScreen->setGeometry(VideoScreen->x(),VideoScreen->y(),OWidth,OHeight);

            VToolb->TimeSlider->setMaximum(Duration);

            VToolb->TotalTimeLabel->setText(IntToTime(Duration));
        }
    }

    void VPlayVideo()
    {
        VideoPlayer->play();
    }
    void VPauseVideo()
    {
        VideoPlayer->pause();
    }
    void VStopVideo()
    {
        VideoPlayer->stop();
        //VideoPlayer->pause();
        /*if(VideoPlayer->position() != VideoPlayer->duration())
        {
            VideoPlayer->pause();
            VideoPlayer->setPosition(0);
            VideoPlayer->pause();
        }*/
    }

    void VelapsedTimeChanged(qint64 mseconds)
    {
        int Seconds = mseconds / 1000;

        VToolb->ElapsedTimeLabel->setText(IntToTime(Seconds));
        VToolb->TimeSlider->setValue(Seconds);
    }

    void VchangePosition(int mseconds)
    {
        if(VideoPlayer->state() == QMediaPlayer::PausedState)
        {
            VideoPlayer->setPosition(mseconds * 1000);
        }
    }

    void VplayAfterChangePosition()
    {
        if(VideoPlayer->state() == QMediaPlayer::PausedState)
        {
            VideoPlayer->play();
        }
    }

    void VpauseBeforeChangePosition()
    {
        if(VideoPlayer->state() != QMediaPlayer::PausedState)
        {
            VideoPlayer->pause();
        }
    }

    void VchangeVolume(int vol)
    {
        VideoPlayer->setVolume(vol);

        VToolb->VolumeLabel->setText(QString::number(vol) + "%");
    }
    //End of Video Slots and Settings





    //Image viewer settings
    //Code of PreviousImage() and NextImage() is influenced by GenCore software
    //http://gen-core.sourceforge.net/
    //The code actually gets the list of file in the same folder of the current image and then selects the one before it
    //or after it depending on the button clicked.
    void NextImage()
    {

        if(ImageShiftCounter == -1)
        {
            Dirlist.clear();
            DirStack.clear();
            ScanDirectory.clear();

            QFileInfo FileInfo(CurrentImage);

            FileString = FileInfo.absolutePath();
            Dirlist.push_back(FileString);
            DirStack.push_back(Dirlist.takeFirst());
            ScanDirs.setPath(DirStack);
            ScanDirectory = ScanDirs.entryList(QDir::Files | QDir::Hidden | QDir::NoDotAndDotDot | QDir::NoSymLinks,QDir::Name);

            for (int i = 0; i < ScanDirectory.count(); i++)
            {
                QString FileStack(FileString + "/" + ScanDirectory.at(i));
                if(FileStack == CurrentImage)
                {
                    ImageShiftCounter = i;
                    break;
                }
            }

        }

        while(!ScanDirectory.isEmpty())
        {
            QString FileName;
            if(ImageShiftCounter < ScanDirectory.count() - 1)
            {
                ImageShiftCounter += 1;
                FileName.push_back(ScanDirectory.at(ImageShiftCounter));
            }
            else if(ImageShiftCounter == ScanDirectory.count() - 1)
            {
                ImageShiftCounter = 0;
                FileName.push_back(ScanDirectory.at(ImageShiftCounter));
            }




             QString FileStack(FileString + "/" + FileName);

             if(FileStack != CurrentImage)
             {
                if(FileName.endsWith(".jpg",Qt::CaseInsensitive) || FileName.endsWith(".jpeg",Qt::CaseInsensitive))
                {
                    AIO_ProcessMedia(FileStack, JPG);
                    break;
                }
                else if(FileName.endsWith(".png",Qt::CaseInsensitive))
                {
                    AIO_ProcessMedia(FileStack, PNG);
                    break;
                }
                else if(FileName.endsWith(".gif",Qt::CaseInsensitive))
                {
                    AIO_ProcessMedia(FileStack, GIF);
                    break;
                }
                else if(FileName.endsWith(".bmp",Qt::CaseInsensitive))
                {
                    AIO_ProcessMedia(FileStack, BMP);
                    break;
                }
             }
             else break;
        }

    }

    void PreviousImage()
    {

        if(ImageShiftCounter == -1)
        {
            Dirlist.clear();
            DirStack.clear();
            ScanDirectory.clear();

            QFileInfo FileInfo(CurrentImage);

            FileString = FileInfo.absolutePath();
            Dirlist.push_back(FileString);
            DirStack.push_back(Dirlist.takeFirst());
            ScanDirs.setPath(DirStack);
            ScanDirectory = ScanDirs.entryList(QDir::Files | QDir::Hidden | QDir::NoDotAndDotDot | QDir::NoSymLinks,QDir::Name);

            for (int i = 0; i < ScanDirectory.count(); i++)
            {
                QString FileStack(FileString + "/" + ScanDirectory.at(i));
                if(FileStack == CurrentImage)
                {
                    ImageShiftCounter = i;
                    break;
                }
            }
        }

        while(!ScanDirectory.isEmpty())
        {
            QString FileName;

            if(ImageShiftCounter > 0)
            {
                ImageShiftCounter -= 1;
                FileName.push_back(ScanDirectory.at(ImageShiftCounter));
            }
            else if(ImageShiftCounter <= 0)
            {
                ImageShiftCounter = ScanDirectory.count() - 1;
                FileName.push_back(ScanDirectory.at(ImageShiftCounter));
            }




             QString FileStack(FileString + "/" + FileName);

             if(FileStack != CurrentImage)
             {
                if(FileName.endsWith(".jpg",Qt::CaseInsensitive) || FileName.endsWith(".jpeg",Qt::CaseInsensitive))
                {
                    AIO_ProcessMedia(FileStack, JPG);
                    break;
                }
                else if(FileName.endsWith(".png",Qt::CaseInsensitive))
                {
                    AIO_ProcessMedia(FileStack, PNG);
                    break;
                }
                else if(FileName.endsWith(".gif",Qt::CaseInsensitive))
                {
                    AIO_ProcessMedia(FileStack, GIF);
                    break;
                }
                else if(FileName.endsWith(".bmp",Qt::CaseInsensitive))
                {
                    AIO_ProcessMedia(FileStack, BMP);
                    break;
                }
             }
             else break;
        }

    }
    //End of Image Viewer settings

    //Experimental code of the MiniImage and MiniFrame featured (Unimplemented)
    /*
    void MovePicture(QPoint pos)
    {
        Screen->setGeometry(-1 * (ScaleImage->MiniFrame->x() * (Screen->width() / ScaleImage->width())),(-1 * (ScaleImage->MiniFrame->y() * (Screen->height() / ScaleImage->height()))),Screen->width(),Screen->height());
    }*/

    //Media functions

    void processVideo(MMFiles format,QString Filename)
    {
        //Everything is done using Signals and Slots
        //If Video Toolbar is not already setup, setup it
        if(VToolb->isHidden())
        {
            VToolb->setGeometry(0,this->height() - ToolBarSpace,this->width(),ToolBarSpace);
            VToolb->SetupToolbar();

            VToolb->VolumeSlider->setMaximum(100);
            VToolb->VolumeSlider->setValue(VideoPlayer->volume());

            VToolb->show();
        }
        //End of Video Toolbar setup

        CurrentVideo = Filename;

        //Hide the Image Screen
        Screen->setHidden(true);

        //Load the media and setup the video screen
        VideoPlayer->setMedia(QUrl::fromLocalFile(Filename));

        VideoPlayer->setVideoOutput(VideoScreen);

        VideoScreen->show();

        VideoPlayer->play();
    }

    void processImage(MMFiles format,QString Filename)
    {
        if(format == GIF)
        {
            this->setWindowTitle("qAllInOne - Loading image...");

            //Load the GIF as a QMovie
            //Note Loading the file as QFile and then getting its name will return a Cross-platfrom filename
            QFile ImageFile(Filename);
            ImageFile.open(QFile::ReadOnly);
            ImageFile.close();

            QMovie * MvImage = new QMovie(ImageFile.fileName(),QByteArray(),ui->MainWidget);
            //Set the GIF Image to the label
            Screen->setMovie(MvImage);
            CurrentImage = ImageFile.fileName();

            //Start playing the Gif Image
            MvImage->start();
            //If the GIF Image is bigger than 1280x800 scale it to 1280x800
            if(MvImage->currentImage().width() > 1280 || MvImage->currentImage().height() > 800)
            {
                QSize ScreenSize(1280,800);
                MvImage->setScaledSize(ScreenSize);
            }

            //Process the scales of the image (For zooming)

            QImage ORImage;
            ORImage = MvImage->currentImage();

            //Add the original image first to be at index 0

            ScaledImageList.push_back(ORImage);

            //Scale the image to increase the width and height with the width and height of the original image divided by 8
            //Do this process 9 times and the scaled image to the ScaledImageList each time

            for(int i=0;i < 9; i++)
            {
                ORImage = ORImage.scaled(ORImage.width() + MvImage->currentImage().width()/8,ORImage.height() + MvImage->currentImage().height()/8,Qt::KeepAspectRatio,Qt::SmoothTransformation);
                ScaledImageList.push_back(ORImage);
            }

            //End of scale processing

            //Center MainWindow
            QDesktopWidget *desktop = QApplication::desktop();
            int CenterWidth = ((desktop->width() - (MvImage->currentImage().width() + ImageMargin)) / 2);
            int CenterHeight = ((desktop->height() - (MvImage->currentImage().height())) / 2);

            //Set MainWindow in Center and add a 60x60 Margin
            //If MainWindow is not maximized
            if(!this->isMaximized())
            {
                //Lock MainWindow sizes
                this->setMinimumSize(MvImage->currentImage().width() + ImageMargin,MvImage->currentImage().height() + ToolBarSpace + ui->menuBar->height());
                //Set MainWindow to Center and set the size with a 60x60 margin
                this->setGeometry(CenterWidth,CenterHeight - (ToolBarSpace / 2),MvImage->currentImage().width() + ImageMargin,MvImage->currentImage().height() + ToolBarSpace + ui->menuBar->height());
            }

            //If Image Toolbar is not already setup then setup it
            if(Toolb->isHidden())
            {
                Toolb->setGeometry(0,this->height() - ToolBarSpace,this->width(),ToolBarSpace);
                Toolb->SetupToolbar();
                Toolb->show();
            }

            //Set the MainWidget to the center of MainWindow
            ui->MainWidget->setGeometry(0,0,this->width(),this->height() - ToolBarSpace - ui->menuBar->height());
            //Scale the Screen Label to the same size of the GIF image
            Screen->setGeometry((ui->MainWidget->width() / 2) - (MvImage->currentImage().width() / 2),(ui->MainWidget->height() / 2) - (MvImage->currentImage().height() / 2),MvImage->currentImage().width(),MvImage->currentImage().height());

            //Set MainWindow title and add file path
            this->setWindowTitle("qAllInOne - (" + Filename + ")");
            Screen->show();
            VideoScreen->setHidden(true);
        }
        else
        {
            this->setWindowTitle("qAllInOne - Loading image...");

            //Intialize QImage and load the file
            QImage PrcImage;
            //Note Loading the file as QFile and then getting its name will return a Cross-platfrom filename
            QFile ImageFile(Filename);
            ImageFile.open(QFile::ReadOnly);
            ImageFile.close();

            PrcImage.load(ImageFile.fileName(),0);
            CurrentImage = ImageFile.fileName();

            //If the image height is larger than 768 scale it to 768
            if(PrcImage.height() > 768)
            {
                PrcImage = PrcImage.scaledToHeight(768,Qt::SmoothTransformation);
            }

            //Process the scales of the image (For zooming)

            QImage ORImage;
            ORImage = PrcImage;

            //Add the original image first to be at index 0

            ScaledImageList.push_back(ORImage);

            //Scale the image to increase the width and height with the width and height of the original image divided by 8
            //Do this process 9 times and the scaled image to the ScaledImageList each time

            for(int i=0;i < 9; i++)
            {
                ORImage = ORImage.scaled(ORImage.width() + PrcImage.width()/8,ORImage.height() + PrcImage.height()/8,Qt::KeepAspectRatio,Qt::SmoothTransformation);
                ScaledImageList.push_back(ORImage);
            }

            //End of scale processing


            //Center MainWindow
            QDesktopWidget *desktop = QApplication::desktop();
            int CenterWidth = ((desktop->width() - (PrcImage.width() + ImageMargin)) / 2);
            int CenterHeight = ((desktop->height() - (PrcImage.height())) / 2);

            //Set MainWindow in Center and add a 60x60 Margin
            //If MainWindow is not maximized
            if(!this->isMaximized())
            {
                //Lock MainWindow sizes
                this->setMinimumSize(PrcImage.width() + ImageMargin,PrcImage.height() + ToolBarSpace + ui->menuBar->height());
                //Set MainWindow to Center and set the size with a 60x60 margin
                this->setGeometry(CenterWidth,CenterHeight - (ToolBarSpace / 2),PrcImage.width() + ImageMargin,PrcImage.height() + ToolBarSpace + ui->menuBar->height());
            }

            //If Image Toolbar is not already setup then setup it
            if(Toolb->isHidden())
            {
                Toolb->setGeometry(0,this->height() - ToolBarSpace,this->width(),ToolBarSpace);
                Toolb->SetupToolbar();
                Toolb->show();
            }

            //Put the MainWidget in the center and resize it to the Image's size
            ui->MainWidget->setGeometry(0,0,this->width(),this->height() - ToolBarSpace - ui->menuBar->height());

            //Resize the Scroll Screen and Screen Label to the Image size and load the image to it
            Screen->setGeometry((ui->MainWidget->width() / 2) - (PrcImage.width() / 2),(ui->MainWidget->height() / 2) - (PrcImage.height() / 2),PrcImage.width(),PrcImage.height());

            Screen->setPixmap(QPixmap::fromImage(PrcImage,Qt::AutoColor));

            //Set MainWindow title and add file path
            this->setWindowTitle("qAllInOne - (" + Filename + ")");
            Screen->show();
            VideoScreen->setHidden(true);
        }
    }

    void processAudio(MMFiles format,QString Filename)
    {
        //Everything is done using Signals and Slots
        //If Audio Toolbar is not already setup, setup it
        if(AToolb->isHidden())
        {
            AToolb->setGeometry(0,this->height() - ToolBarSpace,this->width(),ToolBarSpace);
            AToolb->SetupToolbar();

            AToolb->VolumeSlider->setMaximum(100);
            AToolb->VolumeSlider->setValue(AudioPlayer->volume());

            AToolb->show();
        }
        //End of Video Toolbar setup

        CurrentAudio = Filename;

        //Hide the Image Screen
        Screen->setHidden(true);

        //Load the media
        AudioPlayer->setMedia(QUrl::fromLocalFile(Filename));

        AudioPlayer->play();
    }





private:
    Ui::MainWindow *ui;
    void resizeEvent(QResizeEvent* event);
    void wheelEvent(QWheelEvent *event);

private slots:
    void showAbout();
    void openMedia();
    void reportBug();
    void visitHomepage();
};

#endif // MAINWINDOW_H
