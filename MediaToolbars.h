/*qAllInOne is a free open-source software built using Qt to provide users an All-In-One media player, so it can view images, play videos and audio.
qAllInOne Copyright (C) 2013  Mahmoud Jaoune

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MEDIATOOLBARS_H
#define MEDIATOOLBARS_H

#include <QtWidgets>
#include <QPushButton>
#include <QMainWindow>
#include <QLabel>
#include <QSlider>

//Pictures Toolbar
class ImageViewerToolBar : public QWidget
{
    Q_OBJECT

public:
    QPushButton * PrevButton;
    QPushButton * NextButton;
    QLabel * Background;


    explicit ImageViewerToolBar(QWidget * parent)
    {
        setParent(parent);
    }

    void SetupToolbar()
    {
        Background = new QLabel(this);
        Background->setGeometry(0,0,this->width(),this->height());
        Background->setStyleSheet("* { background-color: rgb(240, 240, 240); }");

        PrevButton = new QPushButton(this);

        PrevButton->setText("Prev");
        PrevButton->setGeometry(this->width() / 2 - 50 - (66/2),(this->height() / 2) - (25/2),66,25);
        PrevButton->show();

        connect(PrevButton,SIGNAL(clicked()),this->parent(),SLOT(PreviousImage()));

        NextButton = new QPushButton(this);

        NextButton->setText("Next");
        NextButton->setGeometry(this->width() / 2 + 50 - (66/2),(this->height() / 2) - (25/2),66,25);
        NextButton->show();

        connect(NextButton,SIGNAL(clicked()),this->parent(),SLOT(NextImage()));
    }

public slots:
    void RepositionObjects()
    {
        PrevButton->setGeometry(this->width() / 2 - 50 - (66/2),(this->height() / 2) - (25/2),66,25);
        NextButton->setGeometry(this->width() / 2 + 50 - (66/2),(this->height() / 2) - (25/2),66,25);
        Background->setGeometry(0,0,this->width(),this->height());
    }

};


//Video Toolbar
class VideoToolBar : public QWidget
{
    Q_OBJECT

public:
    QPushButton * PlayButton;
    QPushButton * StopButton;
    QPushButton * PauseButton;
    QSlider * TimeSlider;
    QSlider * VolumeSlider;
    QLabel * Background;

    QLabel * ElapsedTimeLabel;
    QLabel * TotalTimeLabel;
    QLabel * VolumeLabel;


    explicit VideoToolBar(QWidget * parent)
    {
        setParent(parent);
    }

    void SetupToolbar()
    {
        Background = new QLabel(this);
        Background->setGeometry(0,0,this->width(),this->height());
        Background->setStyleSheet("* { background-color: rgb(240, 240, 240); }");

        PlayButton = new QPushButton(this);

        PlayButton->setText("Play");
        PlayButton->setGeometry(10,(this->height() / 2) + 10 - (25/2),35,25);
        PlayButton->show();

        connect(PlayButton,SIGNAL(clicked()),this->parent(),SLOT(VPlayVideo()));

        StopButton = new QPushButton(this);

        StopButton->setText("Stop");
        StopButton->setGeometry(45,(this->height() / 2) + 10 - (25/2),35,25);
        StopButton->show();

        connect(StopButton,SIGNAL(clicked()),this->parent(),SLOT(VStopVideo()));

        PauseButton = new QPushButton(this);

        PauseButton->setText("Pause");
        PauseButton->setGeometry(80,(this->height() / 2) + 10 - (25/2),35,25);
        PauseButton->show();

        connect(PauseButton,SIGNAL(clicked()),this->parent(),SLOT(VPauseVideo()));

        ElapsedTimeLabel = new QLabel(this);
        ElapsedTimeLabel->setGeometry(5,5,50,20);
        ElapsedTimeLabel->setAlignment(Qt::AlignHCenter);
        ElapsedTimeLabel->show();

        TotalTimeLabel = new QLabel(this);
        TotalTimeLabel->setGeometry(5,5,35,20);

        TimeSlider = new QSlider(this);
        TimeSlider->setGeometry(ElapsedTimeLabel->width() + 7,5,this->width() - ElapsedTimeLabel->width() - 50 - 30,20);
        TimeSlider->show();
        TimeSlider->setOrientation(Qt::Horizontal);

        connect(TimeSlider,SIGNAL(sliderPressed()),this->parent(),SLOT(VpauseBeforeChangePosition()));
        connect(TimeSlider,SIGNAL(valueChanged(int)),this->parent(),SLOT(VchangePosition(int)));
        connect(TimeSlider,SIGNAL(sliderReleased()),this->parent(),SLOT(VplayAfterChangePosition()));

        TotalTimeLabel->setGeometry(this->width() - 65,5,35,20);
        TotalTimeLabel->show();
        TotalTimeLabel->setAlignment(Qt::AlignHCenter);

        VolumeLabel = new QLabel(this);
        VolumeLabel->setGeometry(this->width() - 45,StopButton->y(),35,20);
        VolumeLabel->show();
        VolumeLabel->setAlignment(Qt::AlignHCenter);

        VolumeSlider = new QSlider(this);
        VolumeSlider->setGeometry(this->width() - 65 - 45,StopButton->y(),60,20);
        VolumeSlider->show();
        VolumeSlider->setOrientation(Qt::Horizontal);

        connect(VolumeSlider,SIGNAL(valueChanged(int)),this->parent(),SLOT(VchangeVolume(int)));
    }

public slots:
    void RepositionObjects()
    {
        PlayButton->setGeometry(10,(this->height() / 2) + 10 - (25/2),35,25);
        StopButton->setGeometry(45,(this->height() / 2) + 10 - (25/2),35,25);
        PauseButton->setGeometry(80,(this->height() / 2) + 10 - (25/2),35,25);
        ElapsedTimeLabel->setGeometry(5,5,35,20);
        TimeSlider->setGeometry(ElapsedTimeLabel->width() + 7,5,this->width() - ElapsedTimeLabel->width() - 50 - 30,20);
        TotalTimeLabel->setGeometry(this->width() - 65,5,35,20);
        VolumeLabel->setGeometry(this->width() - 45,StopButton->y(),35,20);
        VolumeSlider->setGeometry(this->width() - 65 - 45,StopButton->y(),60,20);
        Background->setGeometry(0,0,this->width(),this->height());
    }

};

//Audio Toolbar
class AudioToolBar : public QWidget
{
    Q_OBJECT

public:
    QPushButton * PlayButton;
    QPushButton * StopButton;
    QPushButton * PauseButton;
    QSlider * TimeSlider;
    QSlider * VolumeSlider;
    QLabel * Background;

    QLabel * ElapsedTimeLabel;
    QLabel * TotalTimeLabel;
    QLabel * VolumeLabel;


    explicit AudioToolBar(QWidget * parent)
    {
        setParent(parent);
    }

    void SetupToolbar()
    {
        Background = new QLabel(this);
        Background->setGeometry(0,0,this->width(),this->height());
        Background->setStyleSheet("* { background-color: rgb(240, 240, 240); }");

        PlayButton = new QPushButton(this);

        PlayButton->setText("Play");
        PlayButton->setGeometry(10,(this->height() / 2) + 10 - (25/2),35,25);
        PlayButton->show();

        connect(PlayButton,SIGNAL(clicked()),this->parent(),SLOT(APlayVideo()));

        StopButton = new QPushButton(this);

        StopButton->setText("Stop");
        StopButton->setGeometry(45,(this->height() / 2) + 10 - (25/2),35,25);
        StopButton->show();

        connect(StopButton,SIGNAL(clicked()),this->parent(),SLOT(AStopVideo()));

        PauseButton = new QPushButton(this);

        PauseButton->setText("Pause");
        PauseButton->setGeometry(80,(this->height() / 2) + 10 - (25/2),35,25);
        PauseButton->show();

        connect(PauseButton,SIGNAL(clicked()),this->parent(),SLOT(APauseVideo()));

        ElapsedTimeLabel = new QLabel(this);
        ElapsedTimeLabel->setGeometry(5,5,50,20);
        ElapsedTimeLabel->setAlignment(Qt::AlignHCenter);
        ElapsedTimeLabel->show();

        TotalTimeLabel = new QLabel(this);
        TotalTimeLabel->setGeometry(5,5,35,20);

        TimeSlider = new QSlider(this);
        TimeSlider->setGeometry(ElapsedTimeLabel->width() + 7,5,this->width() - ElapsedTimeLabel->width() - 50 - 30,20);
        TimeSlider->show();
        TimeSlider->setOrientation(Qt::Horizontal);

        connect(TimeSlider,SIGNAL(sliderPressed()),this->parent(),SLOT(ApauseBeforeChangePosition()));
        connect(TimeSlider,SIGNAL(valueChanged(int)),this->parent(),SLOT(AchangePosition(int)));
        connect(TimeSlider,SIGNAL(sliderReleased()),this->parent(),SLOT(AplayAfterChangePosition()));

        TotalTimeLabel->setGeometry(this->width() - 65,5,35,20);
        TotalTimeLabel->show();
        TotalTimeLabel->setAlignment(Qt::AlignHCenter);

        VolumeLabel = new QLabel(this);
        VolumeLabel->setGeometry(this->width() - 45,StopButton->y(),35,20);
        VolumeLabel->show();
        VolumeLabel->setAlignment(Qt::AlignHCenter);

        VolumeSlider = new QSlider(this);
        VolumeSlider->setGeometry(this->width() - 65 - 45,StopButton->y(),60,20);
        VolumeSlider->show();
        VolumeSlider->setOrientation(Qt::Horizontal);

        connect(VolumeSlider,SIGNAL(valueChanged(int)),this->parent(),SLOT(AchangeVolume(int)));
    }

public slots:
    void RepositionObjects()
    {
        PlayButton->setGeometry(10,(this->height() / 2) + 10 - (25/2),35,25);
        StopButton->setGeometry(45,(this->height() / 2) + 10 - (25/2),35,25);
        PauseButton->setGeometry(80,(this->height() / 2) + 10 - (25/2),35,25);
        ElapsedTimeLabel->setGeometry(5,5,35,20);
        TimeSlider->setGeometry(ElapsedTimeLabel->width() + 7,5,this->width() - ElapsedTimeLabel->width() - 50 - 30,20);
        TotalTimeLabel->setGeometry(this->width() - 65,5,35,20);
        VolumeLabel->setGeometry(this->width() - 45,StopButton->y(),35,20);
        VolumeSlider->setGeometry(this->width() - 65 - 45,StopButton->y(),60,20);
        Background->setGeometry(0,0,this->width(),this->height());
    }

};


#endif // MEDIATOOLBARS_H
