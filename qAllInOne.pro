#-------------------------------------------------
#
# Project created by QtCreator 2013-11-06T14:34:55
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qAllInOne
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp

QTPLUGIN += dsengine

HEADERS  += mainwindow.h \
    ImageViewerContents.h \
    MediaToolbars.h \
    Defines.h

FORMS    += mainwindow.ui

RESOURCES += \
    Resources.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../Qt/Qt/qtbase/plugins/mediaservice/ -ldsengine
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../Qt/Qt/qtbase/plugins/mediaservice/ -ldsengined
else:unix:!macx: LIBS += -L$$PWD/../../../../Qt/Qt/qtbase/plugins/mediaservice/ -ldsengine

INCLUDEPATH += $$PWD/../../../../Qt/Qt/qtbase/plugins/mediaservice
DEPENDPATH += $$PWD/../../../../Qt/Qt/qtbase/plugins/mediaservice

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../Qt/Qt/qtbase/plugins/mediaservice/libdsengine.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../Qt/Qt/qtbase/plugins/mediaservice/libdsengined.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../Qt/Qt/qtbase/plugins/mediaservice/dsengine.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../Qt/Qt/qtbase/plugins/mediaservice/dsengined.lib
else:unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../Qt/Qt/qtbase/plugins/mediaservice/libdsengine.a
