/*qAllInOne is a free open-source software built using Qt to provide users an All-In-One media player, so it can view images, play videos and audio.
qAllInOne Copyright (C) 2013  Mahmoud Jaoune

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    //Arguments
    //Intializing Arguments
    QString ArgumentStr = argv[1];


        /*QFile MediaFile;
        MediaFile.setFileName(ArgumentStr);
        MediaFile.open(QFile::ReadOnly);
        MediaFile.close();*/

        QString Fname = ArgumentStr;


        //Extension Check and MultiMedia processing

        if(Fname.endsWith(".jpg",Qt::CaseInsensitive) || Fname.endsWith(".jpeg",Qt::CaseInsensitive))
        {
            w.AIO_ProcessMedia(Fname,w.JPG);
        }
        else if(Fname.endsWith(".png",Qt::CaseInsensitive))
        {
            w.AIO_ProcessMedia(Fname,w.PNG);
        }
        else if(Fname.endsWith(".bmp",Qt::CaseInsensitive))
        {
            w.AIO_ProcessMedia(Fname,w.BMP);
        }
        else if(Fname.endsWith(".gif",Qt::CaseInsensitive))
        {
            w.AIO_ProcessMedia(Fname,w.GIF);
        }
        else if(Fname.endsWith(".wmv",Qt::CaseInsensitive))
        {
            w.AIO_ProcessMedia(Fname,w.WMV);
        }
        else if(Fname.endsWith(".mp4",Qt::CaseInsensitive))
        {
            w.AIO_ProcessMedia(Fname,w.MP4);
        }
        else if(Fname.endsWith(".avi",Qt::CaseInsensitive))
        {
            w.AIO_ProcessMedia(Fname,w.AVI);
        }
        else if(Fname.endsWith(".mp3",Qt::CaseInsensitive))
        {
            w.AIO_ProcessMedia(Fname,w.MP3);
        }


    return a.exec();
}
