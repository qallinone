/*qAllInOne is a free open-source software built using Qt to provide users an All-In-One media player, so it can view images, play videos and audio.
qAllInOne Copyright (C) 2013  Mahmoud Jaoune

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Defines.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    //Setting Public Variables
    ImageShiftCounter = -1;
    ZoomCounter = 0; //Set Zoom to 0; since the image is not yet zoomed in

    //Spaces and Margins
    ToolBarSpace = 64;
    ImageMargin = 100;

    //UI setup and stuff
    ui->setupUi(this);
    setMouseTracking(true);

    //Align MainWidget to center of MainWindow
    ui->MainWidget->setGeometry(((this->width() / 2) - (ui->MainWidget->width() / 2)),(this->height() / 2) - (ui->MainWidget->height() / 2) - ToolBarSpace,ui->MainWidget->width(),ui->MainWidget->height() + ToolBarSpace);

    ui->MainWidget->setStyleSheet("* { background-color: rgb(33, 33, 33); }");

    Screen = new ImageScreen(ui->MainWidget);
    Screen->setGeometry(0,0,ui->MainWidget->width(),ui->MainWidget->height());

    Screen->setHidden(true);

    VideoInfo = new QGraphicsVideoItem;


    VideoScreen = new QVideoWidget(this);

    VideoScreen->setStyleSheet("* { background-color: rgb(0, 0, 0); }");
    VideoScreen->setHidden(true);

    //Intialize Media Players so they can be used later
    VideoPlayer = new QMediaPlayer(this);
    AudioPlayer = new QMediaPlayer(this);
    //End of Media Players intialization

    //Intialize Toolbars but disable them until needed
    Toolb = new ImageViewerToolBar(this);
    Toolb->setHidden(true);

    VToolb = new VideoToolBar(this);
    VToolb->setHidden(true);

    AToolb = new AudioToolBar(this);
    AToolb->setHidden(true);
    //End of toolbar settings

    //Set Minimum size of MainWindow to the size of Toolbar and MenuBar
    this->setMinimumSize(450,ToolBarSpace + ui->menuBar->height() + 10);

    //End of UI and components setup


    //Connecting SIGNALS and SLOTS
    connect(VideoPlayer,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),this,SLOT(VMediaStatus(QMediaPlayer::MediaStatus)));
    connect(VideoPlayer,SIGNAL(metaDataAvailableChanged(bool)),this,SLOT(VmetaDataAvailable(bool)));
    connect(VideoPlayer,SIGNAL(positionChanged(qint64)),this,SLOT(VelapsedTimeChanged(qint64)));

    connect(AudioPlayer,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),this,SLOT(AMediaStatus(QMediaPlayer::MediaStatus)));
    connect(AudioPlayer,SIGNAL(metaDataAvailableChanged(bool)),this,SLOT(AmetaDataAvailable(bool)));
    connect(AudioPlayer,SIGNAL(positionChanged(qint64)),this,SLOT(AelapsedTimeChanged(qint64)));

    connect(ui->actionAbout,SIGNAL(triggered()),this,SLOT(showAbout()));
    connect(ui->actionReport_a_bug,SIGNAL(triggered()),this,SLOT(reportBug()));
    connect(ui->actionHomepage,SIGNAL(triggered()),this,SLOT(visitHomepage()));
    connect(ui->actionOpen_media,SIGNAL(triggered()),this,SLOT(openMedia()));
    connect(ui->actionExit,SIGNAL(triggered()),this,SLOT(close()));
    //End of Connecting SIGNALS and SLOTS
}

MainWindow::~MainWindow()
{
    delete ui;
}

//If MainWindow was resized, keep MainWidget in the center of MainWindow
void MainWindow::resizeEvent(QResizeEvent* event)
{
   QMainWindow::resizeEvent(event);

   //Don't change the Label size, but set the MainWidget to the Center
   if(Screen->isHidden() == false)
   {
       ui->MainWidget->setGeometry(0,0,event->size().width(),event->size().height() - ToolBarSpace - ui->menuBar->height());
       Screen->setGeometry((ui->MainWidget->width() / 2) - (Screen->width() / 2),(ui->MainWidget->height() / 2) - (Screen->height() / 2),Screen->width(),Screen->height());
       if(Toolb->isHidden() == false)
       {
           Toolb->setGeometry(0,this->height() - ToolBarSpace,this->width(),ToolBarSpace);
           Toolb->RepositionObjects();
       }
   }
   else if(VideoScreen->isHidden() == false)
   {
       ui->MainWidget->setGeometry(0,0,event->size().width(),event->size().height() - ToolBarSpace - ui->menuBar->height());
       VideoScreen->resize(QSize(event->size().width(),event->size().height() - ToolBarSpace));
       VideoScreen->setGeometry(((event->size().width() / 2) - (VideoScreen->width() / 2)),((event->size().height() / 2) - (VideoScreen->height() / 2) - ui->menuBar->height()),VideoScreen->width(),VideoScreen->height());
       if(VToolb->isHidden() == false)
       {
           VToolb->setGeometry(0,this->height() - ToolBarSpace,this->width(),ToolBarSpace);
           VToolb->RepositionObjects();
       }
   }
   if(!AToolb->isHidden())
   {
       ui->MainWidget->setGeometry(0,0,event->size().width(),event->size().height() - ToolBarSpace - ui->menuBar->height());
       AToolb->setGeometry(0,this->height() - ToolBarSpace,this->width(),ToolBarSpace);
       AToolb->RepositionObjects();
   }

}

void MainWindow::wheelEvent(QWheelEvent* event)
{
    if(!Toolb->isHidden())
    {
        if(event->delta() > 0 && ZoomCounter < 9)
        {
            //Create an image reference to the original image in order to get the original properties from the image
            //Original Image is at index 0
            QImage ORImage = ScaledImageList.at(0);

            //Increase the zoom count by 1
            ZoomCounter += 1;

            //Declare a pixmap to the Image Viewer Pixmap
            //Load the scaled image order from the ScaledImageList
            const QPixmap Scaled1 = QPixmap::fromImage(ScaledImageList.at(ZoomCounter));

            //Set the new scaled pixmap to the Image Viewer and resize it to the same size
            Screen->setPixmap(Scaled1);
            Screen->resize(Scaled1.width(),Scaled1.height());

            //Keep the Image Viewer in center of MainWidget by substracting the added size
            Screen->setGeometry(Screen->x() - (ORImage.width()/8)/2,Screen->y() - (ORImage.height()/8)/2,Screen->width(),Screen->height());



        }
        else if(event->delta() < 0 && ZoomCounter > 0)
        {
            //Create an image reference to the original image in order to get the original properties from the image
            //Original Image is at index 0
            QImage ORImage = ScaledImageList.at(0);

            //Decrease the zoom count by 1
            ZoomCounter -= 1;

            //Declare a pixmap to the Image Viewer Pixmap
            //Load the scaled image order from the ScaledImageList
            const QPixmap Scaled1 = QPixmap::fromImage(ScaledImageList.at(ZoomCounter));

            //Set the new scaled pixmap to the Image Viewer and resize it to the same size
            Screen->setPixmap(Scaled1);
            Screen->resize(Scaled1.width(),Scaled1.height());

            //Keep the Image Viewer in center of MainWidget by adding the substracted size
            Screen->setGeometry(Screen->x() + (ORImage.width()/8)/2,Screen->y() + (ORImage.height()/8)/2,Screen->width(),Screen->height());



        }
        //If Image is back to default size, recenter it in the middle of MainWindow
        if(ZoomCounter == 0)
        {
            Screen->setGeometry((ui->MainWidget->width() / 2) - (Screen->width() / 2),(ui->MainWidget->height() / 2) - (Screen->height() / 2),Screen->width(),Screen->height());

        }
    }
}


int MainWindow::AIO_ProcessMedia(QString Filename, MMFiles Filetype)
{

    //Disable all toolbars before loading media and stop any playing/showing media
    if(VToolb->isHidden() == false)
    {
        VToolb->hide();
        VideoPlayer->stop();
        QVideoWidget* nl = NULL;
        VideoPlayer->setVideoOutput(nl);
    }
    if(AToolb->isHidden() == false)
    {
        AToolb->hide();
        AudioPlayer->stop();
    }
    if(Toolb->isHidden() == false)
    {
        Toolb->hide();
        QImage NULLImage;
        Screen->setPixmap(QPixmap::fromImage(NULLImage));
        ScaledImageList.clear();
        this->setMinimumSize(450,ToolBarSpace + ui->menuBar->height() + 10);
        ZoomCounter = 0;
    }

    //Check Filetype and proccess it depending on its type
    //As of v0.94 the media functions were moved to mainwindow.h and made more portable
    if(Filetype == JPG)
    {
        processImage(JPG,Filename);
    }

    else if(Filetype == PNG)
    {
        processImage(PNG,Filename);
    }

    else if(Filetype == BMP)
    {
        processImage(BMP,Filename);
    }

    else if(Filetype == GIF)
    {
        processImage(GIF,Filename);
    }

    else if(Filetype == WMV)
    {
        processVideo(WMV,Filename);
    }

    else if(Filetype == MP4)
    {
        processVideo(MP4,Filename);
    }

    else if(Filetype == AVI)
    {
        processVideo(AVI,Filename);
    }

    else if(Filetype == MP3)
    {
        processAudio(MP3,Filename);
    }




    return 0;
}

//Convert number to time format MM:SS
QString MainWindow::IntToTime(int seconds)
{
    int Minutes = seconds / 60;
    int Seconds = seconds % 60;
    QString SSeconds;

    QString SMinutes = QString::number(Minutes);

    if(QString::number(Seconds).length() < 2)
    {
        SSeconds = "0" + QString::number(Seconds);
    }
    else if(QString::number(Seconds).length() == 2)
    {
        SSeconds = QString::number(Seconds);
    }

    QString Time = SMinutes + ":" + SSeconds;

    return Time;
}

//Show About dialog
void MainWindow::showAbout()
{
    QDialog * AboutDial = new QDialog();
    AboutDial->setMinimumSize(560,350);
    AboutDial->setMaximumSize(560,350);
    AboutDial->resize(560,350);

    QLabel * Title = new QLabel(AboutDial);
    Title->setGeometry(10,3,155,40);
    Title->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
    Title->setStyleSheet("font: 75 26pt \"Arial\";");
    Title->setText("qAllInOne");

    QLabel * Version = new QLabel(AboutDial);
    Version->setGeometry(159,25,50,20);
    Version->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
    Version->setStyleSheet("font: 7pt \"Times New Roman\";");
    Version->setText(CurrentVersion);

    QTabWidget *AboutWidget = new QTabWidget(AboutDial);
    AboutWidget->setGeometry(0,50,560,350);
    AboutWidget->setMinimumSize(560,350);
    AboutWidget->setMaximumSize(560,350);

    //About widget
    QWidget * About = new QWidget(AboutDial);

    QLabel * AboutT = new QLabel(About);
    AboutT->setGeometry(7,15,350,25);
    AboutT->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
    AboutT->setStyleSheet("font: 75 18pt \"Arial\";");
    AboutT->setText("About");


    QLabel * AboutB = new QLabel(About);
    AboutB->setGeometry(10,50,540,330);
    AboutB->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
    AboutB->setWordWrap(true);
    AboutB->setStyleSheet("font: 13pt \"Arial\";");
    AboutB->setText("qAllInOne is a free open-source software built using Qt to provide users an All-In-One media player, so it can view images, play videos and audio. Instead of having multiple separate software for each media type, qAllInOne aims on providing one software for all media types.\n\nCopyright 2013 - MJaoune\nhttp://qallinone.sourceforge.net/");
    AboutWidget->insertTab(0,About, "About");
    //End of About Widget

    //Author widget
    QWidget * Author = new QWidget(AboutDial);

    QLabel * AuthorT = new QLabel(Author);
    AuthorT->setGeometry(7,15,350,25);
    AuthorT->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
    AuthorT->setStyleSheet("font: 75 18pt \"Arial\";");
    AuthorT->setText("Author");

    QLabel * AuthorL = new QLabel(Author);
    AuthorL->setGeometry(7,50,250,25);
    AuthorL->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
    AuthorL->setStyleSheet("font: 75 13pt \"Arial\";");
    AuthorL->setText("Author & current maintainer:");


    QLabel * AuthorB = new QLabel(Author);
    AuthorB->setGeometry(10,75,540,330);
    AuthorB->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
    AuthorB->setWordWrap(true);
    AuthorB->setStyleSheet("font: 13pt \"Arial\";");
    AuthorB->setText("-Mahmoud Jaoune <MJaoune>\nWebsite: http://mjaoune.users.sourceforge.net/\nE-Mail:mjaoune55@gmail.com");
    AboutWidget->insertTab(1,Author, "Author");
    //End of Author Widget

    //License widget
    QWidget * License = new QWidget(AboutDial);

    QLabel * LicenseT = new QLabel(License);
    LicenseT->setGeometry(7,15,350,25);
    LicenseT->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
    LicenseT->setStyleSheet("font: 75 18pt \"Arial\";");
    LicenseT->setText("License");


    QPlainTextEdit * LicenseB = new QPlainTextEdit(License);
    LicenseB->setGeometry(10,50,530,210);
    LicenseB->setStyleSheet("font: 13pt \"Arial\";");
    LicenseB->setReadOnly(true);
    LicenseB->setPlainText(LicenseText);
    AboutWidget->insertTab(2,License, "License");
    //End of License Widget

    //Contributors widget
    QWidget * Contributor = new QWidget(AboutDial);

    QLabel * ContributorT = new QLabel(Contributor);
    ContributorT->setGeometry(7,15,350,25);
    ContributorT->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
    ContributorT->setStyleSheet("font: 75 18pt \"Arial\";");
    ContributorT->setText("Contributors");


    QPlainTextEdit * ContributorB = new QPlainTextEdit(Contributor);
    ContributorB->setGeometry(10,50,530,210);
    ContributorB->setStyleSheet("font: 13pt \"Arial\";");
    ContributorB->setReadOnly(true);
    ContributorB->setPlainText("List of Contributors:\n\n");
    AboutWidget->insertTab(3,Contributor, "Contributors");
    //End of Contributors Widget

    AboutDial->show();
}

//Open bug page
void MainWindow::reportBug()
{
    QDesktopServices::openUrl(QUrl("https://sourceforge.net/p/qallinone/tickets/", QUrl::TolerantMode));
}

//Open homepage
void MainWindow::visitHomepage()
{
    QDesktopServices::openUrl(QUrl("http://qallinone.sourceforge.net/", QUrl::TolerantMode));
}

void MainWindow::openMedia()
{

    QString Filefilter = "Media Files (*.jpg *.jpeg *.png *.bmp *.gif *.wmv *.mp4 *.avi *.mp3)";
    QString Fname = QFileDialog::getOpenFileName(this,"Open Media File...",QString(),Filefilter);

    if(Fname.endsWith(".jpg",Qt::CaseInsensitive) || Fname.endsWith(".jpeg",Qt::CaseInsensitive))
    {
        AIO_ProcessMedia(Fname,JPG);
    }
    else if(Fname.endsWith(".png",Qt::CaseInsensitive))
    {
        AIO_ProcessMedia(Fname,PNG);
    }
    else if(Fname.endsWith(".bmp",Qt::CaseInsensitive))
    {
        AIO_ProcessMedia(Fname,BMP);
    }
    else if(Fname.endsWith(".gif",Qt::CaseInsensitive))
    {
        AIO_ProcessMedia(Fname,GIF);
    }
    else if(Fname.endsWith(".wmv",Qt::CaseInsensitive))
    {
        AIO_ProcessMedia(Fname,WMV);
    }
    else if(Fname.endsWith(".mp4",Qt::CaseInsensitive))
    {
        AIO_ProcessMedia(Fname,MP4);
    }
    else if(Fname.endsWith(".avi",Qt::CaseInsensitive))
    {
        AIO_ProcessMedia(Fname,AVI);
    }
    else if(Fname.endsWith(".mp3",Qt::CaseInsensitive))
    {
        AIO_ProcessMedia(Fname,MP3);
    }

}
